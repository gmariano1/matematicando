import { Injectable } from '@angular/core';
import { Question } from './models/question.model';
import { QuestionOption } from './models/question-options.model';

@Injectable()
export class QuizService {

  allQuestions: Question[] = [];
  questionIndex = 0;

  constructor() {
    
    let question = new Question();
    question.id = 1;
    question.text = "Quanto é 3 + 3";
    question.options = [
              {id:1, text:"3"},
              {id: 2, text: "5"},
              {id: 3, text: "6"},
              {id: 4, text:"8"}
    ];
    question.correctOptionId = 3;
    this.allQuestions.push(question);

    question = new Question();
    question.id = 2;
    question.text = "Quanto é 3 + 5";
    question.options = [
              {id:1, text:"3"},
              {id: 2, text: "5"},
              {id: 3, text: "6"},
              {id: 4, text:"8"}
    ];
    question.correctOptionId = 4;
    this.allQuestions.push(question);

    question = new Question();
    question.id = 3;
    question.text = "Quanto é 3 + 8";
    question.options = [
              {id:1, text:"5"},
              {id: 2, text: "7"},
              {id: 3, text: "8"},
              {id: 4, text:"11"}
    ];
    question.correctOptionId = 4;
    this.allQuestions.push(question);

    question = new Question();
    question.id = 4;
    question.text = "Quanto é 3 + 7";
    question.options = [
              {id:1, text:"9"},
              {id: 2, text: "10"},
              {id: 3, text: "13"},
              {id: 4, text:"17"}
    ];
    question.correctOptionId = 2;
    this.allQuestions.push(question);
  }

  isLastQuestion() {
    return this.questionIndex == this.allQuestions.length - 1;
  }
    
  getNextQuestion() {
    return this.allQuestions[this.questionIndex++];
  }

  endQuiz() {
    this.questionIndex = 0;
  }

} 
